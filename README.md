# Styleguide Templates

This project contains configuration to setup linting and formatting for various programming languages.
The goal is to have a uniform application of code styles, enforced by CI jobs and aligned with chosen IDEs.

## How it works

The folder `{{ cookiecutter.project_slug }}` contains all boilerplate files that should be present in all repositories that follow this guideline.

It actually is a [cookiecutter](https://github.com/cookiecutter/cookiecutter) template, meaning it supports variables.
That way repos can still adjust have different configuration based on their requirements.

In addition, we're using [cruft](https://cruft.github.io/cruft/) to check and update the template.
A CI job in each repository checks if there's an update in the template repository every time we make changes.

## Prerequisites

- `cruft` (install either via Python, or Homebrew, or use the docker alias: `alias cruft='docker run --rm -it -v ${PWD}:/app -u $(id -u):$(id -g) ghcr.io/cruft/cruft'`)

## Usage

To setup a project that follows the styleguide, run this command in the **parent directory of your repo** to initialize and fill out the variables:

For example, if your repo is stored in `path/to/public/my-foo-repo`, then run this command in `path/to/public`, as it creates/updates the directory beneath, not in the current dir.

```bash
cruft create https://gitlab.com/nxt/public/styleguide-template.git -f --skip .gitlab-ci.yml
```

Note: If the `project_slug` is named differently than your local repo directory, you can move the generated files to their correct location.

### Skip files from update

Some files provided by this template might be modified by the project, and you don't want them to be updated by cruft.

You can tell cruft to ignore these with the `--skip` option:

```bash
cruft create https://gitlab.com/nxt/public/styleguide-template.git -f --skip .gitlab-ci.yml  --skip .config
```

Or you can add a `skip` section to the `.cruft.json` file:

```json
{
  "template": "https://gitlab.com/nxt/public/styleguide-template.git",
  ...
  "skip": [
    ".gitlab-ci.yml",
    ".config"
  ]
}
```

## Development

### Run Cookiecutter

If you have cookiecutter installed, you can run it within this repository to test out if templates work:

```bash
cookiecutter -o test -f ./
```

### Feature Flags

When cruft asks for variables to update, it stores them as string.
Be sure to compare boolean-like flags accordingly, e.g.

```
{% if cookiecutter['myflag']|string|lower == 'true' %}
```

### Hooks

Cruft supports hooks, which are scripts that are executed before and after the update process.

Currently, there is the `post_gen_project.py` hook, which is executed after a project is created,
and is used to clean up files that are not needed in the project.
E.g if you have create a project csharp disabled, it'll remove dotnet-related files and folders.

The hook has some doctests, you can run them with `python hooks/post_gen_project.py`

### CI Jobs

In generall, prefer making CI jobs available as a CI component in https://gitlab.com/nxt/public/gitlab-ci-templates.
This makes it more easy to override job settings compared to variables or feature flags in Cruft.

You can still render a file with the `include`, but avoid putting the CI job definition itself in this template repo.
