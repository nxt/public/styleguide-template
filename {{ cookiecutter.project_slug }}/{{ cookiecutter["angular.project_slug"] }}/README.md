# {{ cookiecutter['angular.project_slug'] }}

## Setup Linters

Prettier, ESLint, and StyleLint are used to enforce code quality and style in our Angular projects.

### Install NPM packages

```bash
npm i -D prettier prettier-plugin-organize-attributes
``````

```bash
npm i -D eslint \
  @eslint/js \
  eslint-import-resolver-typescript \
  typescript-eslint \
  @typescript-eslint/eslint-plugin \
  @typescript-eslint/parser \
  @angular-eslint/builder
```

```bash
npm i -D @angular-eslint/eslint-plugin \
  @angular-eslint/eslint-plugin-template \
  @angular-eslint/schematics \
  @angular-eslint/template-parser \
  angular-eslint
```

```bash
npm i -D eslint-plugin-change-detection-strategy \
  eslint-plugin-import \
  eslint-plugin-lodash-template \
  @ngrx/eslint-plugin \
  eslint-plugin-prettier \
  eslint-plugin-unused-imports \
  @eslint/compat
```

```bash
npm i -D stylelint stylelint-config-standard-scss
```

### Add lint task

In your `angular.json`, add the following lint task :

```json
"lint": {
  "builder": "@angular-eslint/builder:lint",
  "options": {
    "lintFilePatterns": [
      "src/**/*.ts",
      "src/**/*.html"
    ]
  }
}
```

In your package.json, add the following scripts. The exact paths might need to be adjusted based on your project
structure.

```json
"scripts": {
  ...
  "lint": "npm run eslint && npm run prettier && npm run stylelint",
  "eslint": "ng lint",
  "prettier": "npx prettier --check src",
  "stylelint": "npx stylelint 'src/app/**/*.scss' 'src/styles.scss'",
  ...
}
```

## Enable JetBrains IDE plugins

### Prettier

In Rider/IntelliJ, ensure the `Prettier` plugin is installed and enabled.

Under `Settings > Languages & Frameworks > JavaScript > Prettier`, ensure the following settings:

- `Manual prettier configuration` is selected
- The `Prettier package` path should point to the npm package installed in your project's `node_modules` folder
- `Run on 'Reformat Code' action` and `Run on save` are enabled
- `Run for files` glob should at least include `*.ts`, `*.html` and `*.scss`

### Stylelint

In Rider/IntelliJ, ensure the `Stylelint` plugin is installed and enabled.

Under `Settings > Style Sheets > Stylelint`, ensure the following settings:

- `Enable` is selected
- The `Stylelint package` path should point to the npm package installed in your project's `node_modules` folder (
  `/a/b/c/../node_modules/stylelint`)
- `Configuration file` is set to Auto-detect
- `Run for files` glob should at least include `*.css`, and `*.scss`
