import changeDetectionStrategy from 'eslint-plugin-change-detection-strategy';
import prettier from 'eslint-plugin-prettier';
import unusedImports from 'eslint-plugin-unused-imports';
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import angular from 'angular-eslint';
import ngrx from '@ngrx/eslint-plugin/v9';
import importPlugin from 'eslint-plugin-import';
import {includeIgnoreFile} from "@eslint/compat";
import path from "node:path";
import {fileURLToPath} from "node:url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const gitignorePath = path.resolve(__dirname, ".gitignore");

export default tseslint.config(
    //translates gitignore-style patterns into ignores glob patterns.
    includeIgnoreFile(gitignorePath),
    {
        ignores: ['dist/**/*', 'obj/**/*'],
        plugins: {
            'change-detection-strategy': changeDetectionStrategy,
            prettier,
            'unused-imports': unusedImports,
        },
        settings: {
            'import/resolver': {
                typescript: {},
            },
        },
    },
    {
        extends: [
            eslint.configs.recommended,
            ...tseslint.configs.recommended,
            ...tseslint.configs.recommendedTypeChecked,
            tseslint.configs.eslintRecommended,
            ...angular.configs.tsRecommended,
            ...ngrx.configs.all,
            importPlugin.flatConfigs.recommended,
            importPlugin.flatConfigs.typescript,
        ],
        files: ['**/*.ts'],
        ignores: ['**/graphql/.generated.ts'],
        processor: angular.processInlineTemplates,
        languageOptions: {
            parserOptions: {
                project: ['tsconfig.json'],
                createDefaultProgram: true,
            },
        },

        rules: {
            'no-console': ['error'],
            eqeqeq: ['error', 'always'],
            curly: ['error'],

            '@angular-eslint/directive-selector': [
                'error',
                {
                    type: 'attribute',
                    prefix: 'app',
                    style: 'camelCase',
                },
            ],

            '@angular-eslint/component-selector': [
                'error',
                {
                    type: 'element',
                    prefix: ['app', 'lib'],
                    style: 'kebab-case',
                },
            ],

            '@typescript-eslint/explicit-function-return-type': [
                'error',
                {
                    allowExpressions: true,
                },
            ],

            '@typescript-eslint/unbound-method': [
                'error',
                {
                    ignoreStatic: true,
                },
            ],

            'no-unused-vars': 'off',
            '@typescript-eslint/no-unused-vars': [
                'error',
                {
                    argsIgnorePattern: '^_',
                    'caughtErrorsIgnorePattern': '^_',
                },
            ],
            'unused-imports/no-unused-imports': 'error'
        },

    },
    {
        extends: [
            ...angular.configs.templateRecommended,
            ...angular.configs.templateAccessibility
        ],
        files: ['**/*.html'],
        rules: {
            '@angular-eslint/template/i18n': [
                'error',
                {
                    checkId: false,
                    checkText: true,
                    checkAttributes: true,
                    ignoreAttributes: [
                        'autocapitalize',
                        'appendTo',
                        'autocomplete',
                        'buttonIcon',
                        'buttonStyle',
                        'charset',
                        'class',
                        'color',
                        'colspan',
                        'content',
                        'contentStyleClass',
                        'dataKey',
                        'dateFormat',
                        'dir',
                        'enterFromClass',
                        'enterToClass',
                        'field',
                        'fill',
                        'filterBy',
                        'for',
                        'formArrayName',
                        'formControlName',
                        'formGroupName',
                        'headerStyleClass',
                        'height',
                        'href',
                        'icon',
                        'iconDisplay',
                        'iconClass',
                        'iconColor',
                        'iconPos',
                        'iconStyle',
                        'id',
                        'inputId',
                        'key',
                        'lang',
                        'leaveFromClass',
                        'leaveToClass',
                        'list',
                        'locale',
                        'media',
                        'mode',
                        'name',
                        'ngClass',
                        'ngProjectAs',
                        'ngSrc',
                        'optionGroupChildren',
                        'optionLabel',
                        'optionValue',
                        'position',
                        'pSortableColumn',
                        'pStyleClass',
                        'rel',
                        'referrerpolicy',
                        'role',
                        'referrerpolicy',
                        'routerLink',
                        'routerLinkActive',
                        'scrollDirection',
                        'scrollHeight',
                        'selectionMode',
                        'severity',
                        'shape',
                        'src',
                        'stroke',
                        'stroke-width',
                        'style',
                        'styleClass',
                        'svgIcon',
                        'tabindex',
                        'target',
                        'toggleIconStyleClass',
                        'toggleLocation',
                        'toggleLocationMobile',
                        'tooltipPosition',
                        'type',
                        'value',
                        'view',
                        'viewBox',
                        'width',
                        'xmlns',
                        'optionGroupLabel',
                        'classList',
                        'enterActiveClass',
                        'leaveActiveClass',
                    ],
                },
            ],
        },
    },
);
