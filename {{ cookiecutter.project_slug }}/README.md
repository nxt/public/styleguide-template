# {{ cookiecutter.project_name }}

## Cruft

Some of the files in this project are managed by [cruft](https://cruft.github.io/cruft/).

### Prerequisites

- `cruft` (install either via Python, or Homebrew, or use the docker alias:
  `alias cruft='docker run --rm -it -v ${PWD}:/app -u $(id -u):$(id -g) ghcr.io/cruft/cruft'`)

### Update the templates

To update the files managed by cruft, run `cruft update`.

To update the files with a modified parameter, run update like this:

```bash
cruft update --variables-to-update '{"ci.lint_all_codebase":"false"}'
```

### Ignore files from update

Some files provided by this template might be modified by the project, and you don't want them to be updated by cruft.

You can add them to the `skip` section in the `.cruft.json` file:

```json
{
  "template": "https://gitlab.com/nxt/public/styleguide-template.git",
  ...
  "skip": [
    ".gitlab-ci.yml",
    ".config",
    "README.md"
  ]
}
```

## Linters

|                  |                                                                          |
| ---------------- | ------------------------------------------------------------------------ |
| Language         | YAML                                                                     |
| Linter           | [yamllint](https://github.com/adrienverge/yamllint)                      |
| Configuration    | `.linters/yamllint`                                                      |
| Install          | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}` |
| Run              | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}` |
| Verify           | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}` |
| JetBrains Plugin |                                                                          |

{% if cookiecutter['csharp.enabled']|string|lower == 'true' %}
| | |
| ---------------- | ----------------------------------------------------------------- |
| Language | C# |
| Linter | [csharpier](https://csharpier.com/) |
| Configuration | `.editorconfig` |
| Install | `dotnet tool install csharpier` |
| Run | `dotnet csharpier .` |
| Verify | `dotnet csharpier . --check` |
| JetBrains Plugin | [csharpier](https://plugins.jetbrains.com/plugin/18243-csharpier) |
{%- endif %}
{% if cookiecutter['angular.enabled']|string|lower == 'true' %}
| | |
| ---------------- | ----------------------------------------------------------------- |
| Language | Typescript, HTML, SCSS |
| Linter | [Prettier](https://prettier.io/) |
| Configuration | `.prettierrc` |
| Install | see [{{cookiecutter['angular.project_slug']}}](./{{cookiecutter['angular.project_slug']}}/README.md) |
| Run | `npx prettier --write .` |
| Verify | `npx prettier --check .` |
| JetBrains Plugin | [prettier](https://plugins.jetbrains.com/plugin/10456-prettier) |

|                  |                                                                                                      |
| ---------------- | ---------------------------------------------------------------------------------------------------- |
| Language         | Typescript, HTML                                                                                     |
| Linter           | [ESLint](https://eslint.org/)                                                                        |
| Configuration    | `.eslintrc.json`                                                                                     |
| Install          | see [{{cookiecutter['angular.project_slug']}}](./{{cookiecutter['angular.project_slug']}}/README.md) |
| Run              | `ng lint`                                                                                            |
| Verify           |                                                                                                      |
| JetBrains Plugin |                                                                                                      |

|                  |                                                                                                      |
| ---------------- | ---------------------------------------------------------------------------------------------------- |
| Language         | SCSS                                                                                                 |
| Linter           | [StyleLint](https://stylelint.io/)                                                                   |
| Configuration    | `.stylelintrc.json`                                                                                  |
| Install          | see [{{cookiecutter['angular.project_slug']}}](./{{cookiecutter['angular.project_slug']}}/README.md) |
| Run              | `npx stylelint .`                                                                                    |
| Verify           |                                                                                                      |
| JetBrains Plugin |                                                                                                      |

|                  |                                                                                                                                   |
| ---------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| Language         | Markdown (MD)                                                                                                                     |
| Linter           | [MarkdownLint](https://github.com/DavidAnson/markdownlint), [markdownlint-cli](https://github.com/igorshubovych/markdownlint-cli) |
| Configuration    | `.linters/markdownlint.yml`                                                                                                       |
| Install          | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}`                                                          |
| Run              | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}`                                                          |
| Verify           | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}`                                                          |
| JetBrains Plugin |                                                                                                                                   |

{%- endif %}
{% if cookiecutter['java.enabled']|string|lower == 'true' %}
| | |
| ---------------- | ----------------------------------------------------------------- |
| Language | Java |
| Linter | [checkstyle](https://checkstyle.org/) |
| Configuration | `.linters/checkstyle.xml` |
| Install | JetBrains Plugin: Settings -> Tools -> Checkstyle -> Add Config with file `.linters/checkstyle.xml` |
| Run | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}` |
| Verify | `npx mega-linter-runner --flavor {{ cookiecutter['megalinter.flavor']}}` |
| JetBrains Plugin | [checkstyle-IDEA](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea) |
{%- endif %}
