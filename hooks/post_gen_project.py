import os
import shutil
from pathlib import PurePath


def allow_removal(path):
    """
    Only allow removal of relative paths and that are within the current working directory.
    As we don't know where the generated project will be created, absolute paths don't make sense.

    >>> allow_removal('~/foo')
    False
    >>> allow_removal('/foo')
    False
    >>> allow_removal('foo')
    True
    >>> allow_removal('foo/bar')
    True
    >>> allow_removal('../foo')
    False
    """
    abs_path = os.path.abspath(path)
    pure = PurePath(abs_path)
    return not path.startswith('~/') and not os.path.isabs(path) and pure.is_relative_to(os.getcwd())

def remove_unneeded_files():
    REMOVE_PATHS = [
        '{% if not cookiecutter["csharp.enabled"] %}.config{% endif %}',
        '{% if not cookiecutter["java.enabled"] %}.linters/checkstyle.xml{% endif %}',
        '{% if not cookiecutter["angular.enabled"] %}{{ cookiecutter["angular.project_slug"] }}/.prettierrc{% endif %}',
        '{% if not cookiecutter["angular.enabled"] %}{{ cookiecutter["angular.project_slug"] }}/.eslintrc.json{% endif %}',
        '{% if not cookiecutter["angular.enabled"] %}{{ cookiecutter["angular.project_slug"] }}/.stylelintrc.json{% endif %}',
        '{% if not cookiecutter["angular.enabled"] %}{{ cookiecutter["angular.project_slug"] }}/README.md{% endif %}',
    ]
    for path in REMOVE_PATHS:
        path = path.strip()

        if path and os.path.exists(path) and allow_removal(path):
            if os.path.isfile(path):
                os.unlink(path)
            else:
                shutil.rmtree(path)

def add_codeowners_file():
    codeownersPath = 'CODEOWNERS'
    if not os.path.exists(codeownersPath):
        with open(codeownersPath, 'w') as file:
            file.write("TODO please replace this line with '* @<gitlab-username>' to set the the owner of this repository.\n")
        print("Please replace the content of the file 'CODEOWNERS' with the GitLab username(s) of the owner of this repository.\n")

remove_unneeded_files()
add_codeowners_file()

if __name__ == "__main__":
    import doctest
    doctest.testmod()
