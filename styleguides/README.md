# Styleguide

This folder contains high-level style guides for our most relevant programming languages.

It does not aim to be a complete style guide covering every edge case,
but rather a collection of rules and decisions that are relevant to us.

Most of the rules are configured in some configuration file, and enforced by the IDE and linters.
Those detailed settings are not again repeated here.

However, if there are discussions about certain rules, they should be documented here along with the reasoning.
The same goes for rules which are not enforceable by a linting tool.

## General Guidelines

- Rules should be as simple as possible
- Stick to standards of the language
- Formatter default settings should be used where sensible
- Rules should be consistent across languages where not conflicting with the above rules
- Rules should be enforced by the IDE

## Braces around single-line if / for / while blocks

If / for / while blocks should always be surrounded by braces, even if they are single-line.

Examples: 

```csharp
if (condition)
{
    DoSomething();
}
```

## Contradicting standards

C# and Typescript have a few standards which are directly contradicting each other.

For example:

 - Indentation: C# uses 4 spaces, Typescript uses 2 spaces
 - Braces: C# puts opening brace on next line, Typescript puts opening brace on same line

Stick to the standard of each language, because it's easier to enforce and less confusing for developers.