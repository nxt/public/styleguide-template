# YAML

## Tool used for formatting

[yamllint](https://github.com/adrienverge/yamllint)

## How to install

- As part of [megalinter](https://megalinter.io)
- `sudo apt-get install yamllint`
- `brew install yamllint`

See https://yamllint.readthedocs.io/en/stable/quickstart.html#installing-yamllint

## How to configure

see `.linters/yamllint.yml` and https://yamllint.readthedocs.io/en/stable/

## How to run

### IDE

There is a [JetBrains Plugin](https://plugins.jetbrains.com/plugin/15349-yamllint), but it looks very alpha.
It's not required if running via CLI.

### CLI

- `npx mega-linter-runner`
- `yamllint -c .linters/yamllint.yml .`
